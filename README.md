### The structure of Japanese Martial Arts according to Jigoro Kano, and associated competitive formats
  
```mermaid
%%{ init: { 'flowchart': { 'curve': 'stepBefore' } } }%%
flowchart LR
    %% Elements
    judo[Japanese Martial Arts Structure]
	nagewaza["Throwing Technques (Nage waza)"]
	tachiwaza["Standing (Tachi waza)"]
	tewaza["Hand Techniques (Te waza)"]
	ukiwaza["Floating Techniques (Uki waza)"]
	ashiwaza["Foot Techniques (Ashi waza)"]
	koshiwaza["Hip Techniques (Koshi waza)"]
	sutemiwaza["Sacrifice Techniques (Sutemi waza)"]
	katamewaza["Grappling (Katame waza)"]
	shimewaza["Choking Techniques (Shime waza)"]
	osaekomiwaza["Holding Techniques (Osaekomi waza)"]
	kansetsuwaza["Joint Techniques (Kansetsu Waza)"]
	hijiwaza["Elbow Techniques (Hiji waza)"]
    standinghijiwaza["Elbow Techniques (Hiji waza)"]
	tekubiwaza["Wrist Techniques (Tekubi waza)"]
	atemiwaza["Striking Techniques (Atemi waza)"]
	udeatewaza["Hand (Ude ate waza)"]
	kobushiatewaza["Fist Techniques (Kobushi ate waza)"]
	hijiatewaza["Elbow Techniques (Hiji ate waza)"]
	yubisakiatewaza["Fingertip Techniques (Yubisaki ate waza)"]
	tegatanaatewaza["Knife hand techniques (Tegatana ate waza)"]
	ashiatewaza["Kicks (Ashi ate waza)"]
	sekitoatewaza["Ball of Foot Techniques (Sekito ate waza)"]
	kakatoatewaza["Heel Techniques (Kakato ate waza)"]
	jizigashiraatewaza["Knee cap techniques (Jiza gashira ate waza)"]
	atamaatewaza["Head Striking Techniques (Atama ate waza)"]

    %% Groups
    subgraph shiai["Judo Competition"]
        tewaza
        ukiwaza
        ashiwaza
        koshiwaza
        subgraph bjj["BJJ Competition"]
            sutemiwaza
            shimewaza
            osaekomiwaza
            hijiwaza
        end
    end

    subgraph aikido[Aikido Competition]
        standinghijiwaza
        tekubiwaza
        hijiatewaza
        tegatanaatewaza
    end

    subgraph karate[Karate]
        yubisakiatewaza
        subgraph kumite[Karate Competition]
            kobushiatewaza
            sekitoatewaza
            kakatoatewaza
        end
        jizigashiraatewaza
        atamaatewaza
    end

    %% Links
    judo --> nagewaza
    judo --> katamewaza
    judo --> atemiwaza

    nagewaza --> tachiwaza
    nagewaza --> sutemiwaza

    tachiwaza --> tewaza
    tachiwaza --> ukiwaza
    tachiwaza --> ashiwaza
    tachiwaza --> koshiwaza

    katamewaza --> shimewaza
    katamewaza --> osaekomiwaza
    katamewaza --> kansetsuwaza

    kansetsuwaza -. Ground .-> hijiwaza
    kansetsuwaza -. Standing .-> standinghijiwaza
    kansetsuwaza --> tekubiwaza

    atemiwaza --> udeatewaza
    atemiwaza --> ashiatewaza
    atemiwaza ---> atamaatewaza

    udeatewaza --> hijiatewaza
    udeatewaza --> tegatanaatewaza
    udeatewaza --> yubisakiatewaza
    udeatewaza --> kobushiatewaza

    ashiatewaza --> sekitoatewaza
    ashiatewaza --> kakatoatewaza
    ashiatewaza --> jizigashiraatewaza

    %% Styles
    style shiai fill:#000088,color:#fff
    style bjj fill:#0000aa,color:#fff
    style aikido fill:#aa0000,color:#fff
    style karate fill:#888800,color:#000
    style kumite fill:#aaaa00,color:#000
```

